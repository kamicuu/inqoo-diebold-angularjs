```js
function Scope(){
    this.id = 1
    this.parent = null
}
Scope.prototype.count = 0
Scope.prototype.$new = function(){
    const childScope = Object.create(this)
    childScope.$id = ++Scope.prototype.count;
    childScope.parent = this
    return childScope
}

$rootScope = new Scope()
s1 = $rootScope.$new();
s2 = $rootScope.$new();
s2a = s2.$new()

Scope {$id: 3, parent: Scope}
$rootScope.title = 'test'
'test'
s2a.title
'test'
s2a
// Scope {$id: 3, parent: Scope}
//   $id: 3
//   parent: Scope {$id: 2, parent: Scope}
//   [[Prototype]]: Scope
//     $id: 2
//     parent: Scope {id: 1, parent: null, title: 'test'}
//       [[Prototype]]: Scope
//       id: 1
//       parent: null
//       title: "test"
//         [[Prototype]]: Object

// Override parent value with local version (hidding)
s2.title = 's2 title'
// 's2 title'
s2.title
// 's2 title'
s2a.title
// 's2 title'
s1.title
// 'test'
```

## Shared data by object (. - the dot)
```js
$rootScope.user = {name:'alice' }
// {name: 'alice'}
s2a.user
// {name: 'alice'}
s2a.user.name ='bob'
// 'bob'
s1.user
// {name: 'bob'}
s2.user 
// {name: 'bob'}
s2.user = {name:'Upss..'}
// {name: 'Upss..'}
s2a.user
// {name: 'usp..'}
$rootScope.user 
// {name: 'bob'}
```