angular.module('config', [])
  .constant('PAGES', [
    { name: 'users', label: 'Users' },
  ])

const app = angular.module('myApp', [
  'config', 'users','tasks','settings'
])

// Global scope
app.config(function (PAGES) {
  // console.log(PAGES)
})
app.run(function ($rootScope, PAGES) { })



angular.module('myApp').config(function (UsersServiceProvider) {
  UsersServiceProvider.setUsersUrl('http://localhost:3000/users')
})

angular.module('myApp').run(function (UsersService) {
  // UsersService.fetchUsers()
})

app.config(function ($httpProvider) {
  $httpProvider.interceptors.push(function ($q, $rootScope, $timeout) {
    return {
      'request': function (config) {
        config.headers['X-Placki'] = 'true'
        return config;
      },

      'responseError': function (response) {
        $rootScope.message = 'Server error'
        $timeout(() => {
          $rootScope.message = ''
        }, 2000)

        return $q.reject(response);
      }
    };
  });
})

app.filter('yesno', function (/* service */) {
  return function (val) {
    return val ? 'Yes' : 'No'
  }
})


app.controller('AppCtrl', ($scope, PAGES) => {
  $scope.title = 'MyApp'
  $scope.user = { name: 'Guest' }

  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = 'Admin'
    $scope.isLoggedIn = true
  }

  $scope.logout = () => {
    $scope.user.name = 'Guest'
    $scope.isLoggedIn = false
  }

  $scope.pages = PAGES

  $scope.currentPage = $scope.pages[0]

  $scope.goToPage = pageName => {
    $scope.currentPage = $scope.pages.find(p => p.name === pageName)
  }
})
