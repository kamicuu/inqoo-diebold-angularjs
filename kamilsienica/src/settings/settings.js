angular.module("settings", []).controller("SettingsCtrl", ($scope) => {
  const vm = ($scope.settings = {});
  vm.title = "Hello";
});

angular.module('settings').directive('appAlert', function(){
  return{
    scope: {
      msg: '@message',
      type: '@',
      onDismiss: '&'
    },
    template: /*html*/`
    <div class="d-flex justify-content-between align-items-center alert {{$ctrl.alertClassType}}" ng-if="$ctrl.open"> 
      <span>Wiadomość: {{msg}}</span>
      <button class="btn btn-danger" ng-click="$ctrl.close()">X</button>
    </div>`,
    controller($scope) {

      const vm = $scope.$ctrl = {}
      vm.alertClassType = "alert-"+$scope.type
      vm.open = true

      vm.close = () => {
        vm.open = false
        $scope.onDismiss()
      }
      

    }
    
  }
})

angular.module("settings").directive('appHighlight', function ($q) {

  return {
    restrict: 'EACM',
    link(scope, $element, attrs, ctrl){
      console.log('Hello', $element);
      $element.css('color', 'red').html('<p>'+ scope.settings.title + '</p>').on('click', el=> $element.css('color', 'blue'))
    }
  };
});
