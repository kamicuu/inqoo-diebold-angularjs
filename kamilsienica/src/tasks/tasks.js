const tasks = angular.module('tasks', [])


angular.module('tasks')
  .component('tasksPage', {
      scope: {},
      template:/* html */`<div> TasksPage {{$ctrl.title}}
        <div class="row">
          <div class="col">
            <tasks-list tasks="$ctrl.tasks" 
                        on-select="$ctrl.select($event)"></tasks-list>
          </div>
          <div class="col">
            <task-details task="$ctrl.selected" on-cancel="$ctrl.cancel($event, mode)" ng-if="$ctrl.selected" on-edit="$ctrl.isEditing=true">
            </task-details>

            <task-editor on-save="$ctrl.save($event)" task="$ctrl.selected" ng-if="$ctrl.selected && $ctrl.isEditing" on-cancel="$ctrl.isEditing=false"></task-editor>
          </div>
        </div>
      </div>`,
      controllerAs: '$ctrl',
      bindToController: true,
      controller($scope) {
        // const vm = $scope.$ctrl = {}
        this.title = 'Tasks'
        this.mode = 'details'
        this.selected = null
        this.tasks = [{ id: 1, name: 'task1', completed: false }, { id: 2, name: 'task2', completed: true }, { id: 3, name: 'task3', completed: false }]
        this.select = (task) => { this.selected = task }
        this.cancel = (event) => { this.selected = null; this.mode = 'details'; console.log(event);}
        this.edit = (event) => { this.mode = 'edit' }
        this.save = (draft) => {
          this.tasks = this.tasks.map(t => t.id === draft.id ? {...draft} : t)
          this.selected = {...draft}
        }
      }
  })
  .component('tasksList', {
      bindings: { tasks: '=', onSelect: "&" }, // on-select=" zyx = $event.completed "
      template:/* html */`<div> List: <div class="list-group">
        <div class="list-group-item" ng-repeat="task in $ctrl.tasks" 
          ng-class="{active: task.id == $ctrl.selected.id}"
          ng-click="$ctrl.onSelect({$event: task}); $ctrl.selected = task">
        {{$index+1}}. {{task.name}}</div>
      </div></div>`
    
  })
  .component('taskDetails', {
    bindings: { task: '=', onEdit: '&', onCancel: '&' },
    template:/* html */`<div>tasksDetails <dl>
      <dt>Name</dt><dd>{{$ctrl.task.name}}</dd>
    </dl>
      <button class="btn btn-primary" ng-click="$ctrl.onEdit({$event: $ctrl.task})">Edit</button>
      <button class="btn btn-primary" ng-click="$ctrl.onCancel({$event: $ctrl.task})">Cancel</button>
    </div>`,
    controller($rootScope, $http) { } // this == $ctrl
  })
  .component('taskEditor', {
    bindings: { task: '<', onEdit: '&', onCancel: '&', onSave: '&' },
    template:/* html */`<div> taskForm:
        <div class="form-group mb-3">
          <label for="task_name">Name</label>
          <input id="task_name" type="text" class="form-control" 
                ng-model="$ctrl.draft.name">
        </div>

        <div class="form-group mb-3">
          <label for="task_completed">
            <input id="task_completed" ng-model="$ctrl.draft.completed" type="checkbox"> Completed</label>
        </div>

        <button class="btn btn-primary" ng-click="$ctrl.onSave({$event: $ctrl.draft})">Save</button>
        <button class="btn btn-primary" ng-click="$ctrl.onCancel({$event: task, mode:'placki'})">Cancel</button>
      </div>`,
    // controller:'TaskEditorCtrl as $ctrl'
    controller($scope) {
      this.$onInit = () => {
        this.draft = { ...this.task }
      }

      // https://docs.angularjs.org/guide/component#component-based-application-architecture
      this.$onChanges = (changes) => { console.log(changes) }
      this.$doCheck = () => { } // after parent $digest
      this.$postLink = () => { } // after all child DOM is ready

      this.$onDestroy = () => {
        console.log('bye bye!')
      }
    },
    // controllerAs: '$ctrl'
  })














tasks.constant('INITIAL_TASKS', [])
tasks.constant('INITIAL_MODE', 'show')

tasks.controller("TasksCtrl", ($scope) => {

    $scope.selectedTask = {
      id: "123",
      name: "Task 123",
      completed: true,
    };
  
    $scope.tasks = [
      {
        id: "123",
        name: "Task 123",
        completed: true,
      },
      {
        id: "234",
        name: "Task 234",
        completed: false,
      },
      {
        id: "345",
        name: "Task 345",
        completed: true,
      },
    ];
  
    $scope.filteredTasks = [...$scope.tasks ];
  
    $scope.taskName = ""
  
    $scope.mode = "";
  
    $scope.edit = () => {
      $scope.mode = "edit";
      $scope.taskTmp = { ...$scope.selectedTask };
    };
  
    $scope.add = () => {
      $scope.mode = "edit";
      $scope.taskTmp = {};
    };
  
    $scope.save = () => {
      var isEdited = false;
  
      $scope.selectedTask = { ...$scope.taskTmp };
  
      $scope.tasks = $scope.tasks.map((element) => {
        if (element.id === $scope.taskTmp.id) {
          isEdited = true;
          return {...$scope.taskTmp};
        } else return element;
      });
  
      if (!isEdited) {
        $scope.tasks.push({ name: $scope.taskTmp.name!=undefined ?  $scope.taskTmp.name : '', completed: $scope.taskTmp.completed, id: (Math.floor(Math.random() * (100000 - 1)) + 1).toString() });
      }
      $scope.filter()
    };
  
    $scope.cancel = () => {
      $scope.mode = "";
      $scope.taskTmp = {};
    };
  
    $scope.selectTask = (item) => {
      $scope.selectedTask = item;
      $scope.cancel();
    };
  
    $scope.removeTask = (item) => {
      $scope.tasks = $scope.tasks.filter((element) => element.id !== item.id);
      item.id === $scope.selectedTask.id ? ($scope.selectedTask = {}) : $scope.selectedTask;
      $scope.filter()
    };
  
    $scope.filter = () => {
        
      $scope.filteredTasks = $scope.tasks.filter((item) => item.name.toLocaleLowerCase().includes($scope.taskName.toLocaleLowerCase()))
      $scope.coutTask()
    };
  
    $scope.saveOnClick = (event) =>{
        if(event.which === 13){
            $scope.save()
        }
  
    }
  
    $scope.coutTask = () =>{
            
      $scope.completedCount = 0
      $scope.notCompletedCount = 0
  
        $scope.tasks.forEach(item => {
          if(item.completed)
              $scope.completedCount++
          else $scope.notCompletedCount++  
        });
    }
  
    $scope.coutTask()
  
  });