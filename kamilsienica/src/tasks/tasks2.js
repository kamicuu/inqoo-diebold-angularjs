const tasks = angular.module("tasks", []);

tasks.controller("TasksCtrl", ($scope) => {

  $scope.selectedTask = {
    id: "123",
    name: "Task 123",
    completed: true,
  };

  $scope.tasks = [
    {
      id: "123",
      name: "Task 123",
      completed: true,
    },
    {
      id: "234",
      name: "Task 234",
      completed: false,
    },
    {
      id: "345",
      name: "Task 345",
      completed: true,
    },
  ];

  $scope.filteredTasks = [...$scope.tasks ];

  $scope.taskName = ""

  $scope.mode = "";

  $scope.edit = () => {
    $scope.mode = "edit";
    $scope.taskTmp = { ...$scope.selectedTask };
  };

  $scope.add = () => {
    $scope.mode = "edit";
    $scope.taskTmp = {};
  };

  $scope.save = () => {
    var isEdited = false;

    $scope.selectedTask = { ...$scope.taskTmp };

    $scope.tasks = $scope.tasks.map((element) => {
      if (element.id === $scope.taskTmp.id) {
        isEdited = true;
        return {...$scope.taskTmp};
      } else return element;
    });

    if (!isEdited) {
      $scope.tasks.push({ name: $scope.taskTmp.name!=undefined ?  $scope.taskTmp.name : '', completed: $scope.taskTmp.completed, id: (Math.floor(Math.random() * (100000 - 1)) + 1).toString() });
    }
    $scope.filter()
  };

  $scope.cancel = () => {
    $scope.mode = "";
    $scope.taskTmp = {};
  };

  $scope.selectTask = (item) => {
    $scope.selectedTask = item;
    $scope.cancel();
  };

  $scope.removeTask = (item) => {
    $scope.tasks = $scope.tasks.filter((element) => element.id !== item.id);
    item.id === $scope.selectedTask.id ? ($scope.selectedTask = {}) : $scope.selectedTask;
    $scope.filter()
  };

  $scope.filter = () => {
      
    $scope.filteredTasks = $scope.tasks.filter((item) => item.name.toLocaleLowerCase().includes($scope.taskName.toLocaleLowerCase()))
    $scope.coutTask()
  };

  $scope.saveOnClick = (event) =>{
      if(event.which === 13){
          $scope.save()
      }

  }

  $scope.coutTask = () =>{
          
    $scope.completedCount = 0
    $scope.notCompletedCount = 0

      $scope.tasks.forEach(item => {
        if(item.completed)
            $scope.completedCount++
        else $scope.notCompletedCount++  
      });
  }

  $scope.coutTask()

});
