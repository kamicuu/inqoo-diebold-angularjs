const users = angular.module("users", ["user.service"]);

users
  .component("userPage", {
    scope: {},
    template: /*html*/ `
  <div>
    <user-list users="$ctrl.users" selected="$ctrl.selected" on-click="$ctrl.select($event)" delete-user="$ctrl.delete($event)"></user-list>
    <user-edit selected="$ctrl.selected"></user-edit>
  </div>
  `,
    controller(UserService) {
      
      this.refreshUsers = () => {
        UserService.fetchUsers().then(() => {
          this.users = UserService.getUsers();
        });
      }
 
      this.$onInit = () => {
        this.refreshUsers()
        this.selected = {};
      };

      this.select = (id) => {
        this.selected = UserService.getUserById(id);
      };

      this.delete = (id) => {
        debugger
        UserService.deleteUser(id).then(this.refreshUsers());
      };

    },
  })
  .component("userList", {
    bindings: { users: "<", onClick: "&", deleteUser: "&", selected: "<" },
    template: /*html*/ `
    <div class="list-group">
      <div class="list-group-item" ng-repeat="user in $ctrl.users track by $index" ng-class="{active: user.id == $ctrl.selected.id}" ng-click="$ctrl.onClick({$event : user.id})">
        <p class="d-block" >{{$index+1}}. {{user.name}}</p>
        <button class="btn btn-danger d-block" ng-click="$ctrl.deleteUser({$event:user.id})">Remove</button>
      </div>
    </div>
    `
  })
  .component("userDetail", {})
  .component("userEdit", {
    bindings:  { selected: "<" },
    template: /*html*/`
    <form novalidate>
    <div class="form-group mb-3">
      <div>
        <label>Name: <input type="text" required minlength="3" name="username" class="form-control" ng-model="$ctrl.selected.name" /></label>
        <div ng-if="userPage.form['username'].$dirty || userPage.form['username'].$touched || userPage.form['username'].$submitted">
          <span class="text-danger" ng-if="userPage.form['username'].$error.minlength">Name is too short!</span>
          <span class="text-danger" ng-if="userPage.form['username'].$error.required">Name is required!</span>
        </div>
      </div>
      <div>
        <label>Email: <input type="email" name="email" required class="form-control" ng-model="manage.userTmp.email" /></label>
        <div ng-if="userPage.form['email'].$dirty || userPage.form['email'].$touched || userPage.form['email'].$submitted">
          <span class="text-danger" ng-if="userPage.form['email'].$error.email">This is not email!</span>
          <span class="text-danger" ng-if="userPage.form['email'].$error.required">Email is required!</span>
        </div>
      </div>
    </div>
    <button
      class="btn btn-success"
      ng-click="userPage.save(manage.userTmp); manage.editMode=='edit' ? manage.cancel() : ''"
      ng-keypress="saveOnClick($event)"
    >
      Save
    </button>
    <button class="btn btn-warning" ng-click="manage.cancel()">Cancel</button>
  </form>
    `
  })

// users.controller("UsersCtrl", ($scope, UserService) => {
//   const vm = ($scope.userPage = {});

//   vm.refreshUsers = () => {
//     UserService.fetchUsers().then(() => {
//       vm.users = UserService.getUsers();
//     });
//   };

// vm.select = (id) => {
//   vm.selected = UserService.getUserById(id);
// };

//   vm.save = (userDraft) => {
//     if (vm.form.$invalid) {
//       alert("Invalid data in form!");
//       return;
//     }

//     let userExist = false;
//     vm.users.forEach((element) => (element.id === userDraft.id ? (userExist = true) : false));

//     if (userExist) {
//       UserService.updateUser(userDraft).then((resp) => {
//         vm.selected = resp;
//         vm.refreshUsers();
//       });
//     } else {
//       UserService.addUser(userDraft).then(() => {
//         vm.refreshUsers();
//       });
//     }
//   };

//   vm.refreshUsers();
// });

// users.controller("UsersListCtrl", ($scope, UserService) => {
//   const vm = ($scope.list = {});

//   vm.deleteUser = (id) => {
//     UserService.deleteUser(id).then($scope.userPage.refreshUsers());
//   };
// });

// users.controller("UsersDetailCtrl", ($scope) => {
//   const vm = ($scope.detail = {});
// });

// users.controller("UsersManageCtrl", ($scope) => {
//   const vm = ($scope.manage = {});
//   vm.editMode = "none";

//   vm.initializeFormTmpData = (selected) => {
//     vm.userTmp = { ...selected };
//   };

//   vm.edit = () => {
//     vm.editMode = "edit";
//   };

//   vm.add = () => {
//     vm.editMode = "save";
//     vm.userTmp = null;
//   };

//   vm.cancel = () => {
//     vm.editMode = "none";
//   };
// });
