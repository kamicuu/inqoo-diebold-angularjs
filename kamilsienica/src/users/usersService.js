angular.module("user.service", []).service("UserService", function ($rootScope, $q) {

  this.fetchUsers = () => {
    return $q.resolve(
      fetch("http://localhost:3000/users")
        .then((resp) => resp.json())
        .then((resp) => {
          this._users = resp;
        })
    );
  };


  this.getUsers = () => {
    return this._users;
  };

  this.getUserById = (id) => {
    return this._users.find((user) => user.id === id);
  };

  this.updateUser = (user) => {

    return $q.resolve(fetch('http://localhost:3000/users/'+user.id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    }).then(resp => resp.json()))

  }

  this.addUser = (user) => {
    return $q.resolve(fetch('http://localhost:3000/users/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    }).then(resp => resp.json()))
  }

  this.deleteUser = (id) => {
    return $q.resolve(fetch('http://localhost:3000/users/'+id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      }}))   

  }

});
