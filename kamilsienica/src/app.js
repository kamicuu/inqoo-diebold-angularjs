angular.module('config', [])
  
const app = angular.module('myApp', [
  'config', 'users', 'settings', 'ui.router'
])

// Global scope

app.config(function($stateProvider){
  $stateProvider
  .state({
    name: 'users',
    url: '/users',
    template: '<h1>Users</h1>'
  })
  .state({
    name: 'tasks',
    url: '/tasks',
    template: '<h1>Tasks</h1>'
  })
  .state({
    name: 'settings',
    url: '/settings',
    template: '<h1>settings</h1>'
  })
})

app.run(function ($state){
  $state.go('users')
})

app.controller('AppCtrl', ($scope, PAGES) => {
  $scope.title = 'MyApp'
  $scope.user = { name: 'Guest' }

  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = 'Admin'
    $scope.isLoggedIn = true
  }

  $scope.logout = () => {
    $scope.user.name = 'Guest'
    $scope.isLoggedIn = false
  }

  $scope.pages = PAGES

  $scope.currentPage = $scope.pages[1]

  $scope.goToPage = pageName => {
    $scope.currentPage = $scope.pages.find(p => p.name === pageName)
  }
})