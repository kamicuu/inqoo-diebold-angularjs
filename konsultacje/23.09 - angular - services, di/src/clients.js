angular.module('clients', [])
  .service('ClientsService', function ClientsService() {

    this._clients = [
      { id: "123", name: "ClientA 123", address: "ul. A", nip: "123" },
      { id: "234", name: "ClientAB 234", address: "ul. B", nip: "234" },
      { id: "345", name: "ClientABC 345", address: "ul. C", nip: "345" },
    ];
    this.search = (query) => {
      return this._clients.filter(
        c => c.name.toLocaleLowerCase().includes(query.toLocaleLowerCase())
      );
    };

  })
  .controller('ClientLookupCtrl', ($scope, ClientsService) => {
    const vm = $scope.lookup = {};

    vm.query = '';
    vm.results = [];

    const handleClickOutside = (event) => {
      if (event.target.closest('[data-popup]'))
        return;

      vm.results = [];
      $scope.$digest();
    };

    document.addEventListener('click', handleClickOutside);
    $scope.$on('$destroy', () => {
      document.removeEventListener('click', handleClickOutside);
    });

    vm.search = () => {
      console.log('test', vm.query);
      vm.results = ClientsService.search(vm.query);
    };

    vm.select = (klient) => {
      console.log(klient);
      vm.results = [];
      vm.query = klient.name;

      // vm.changeClient(klient)
    };


  });
