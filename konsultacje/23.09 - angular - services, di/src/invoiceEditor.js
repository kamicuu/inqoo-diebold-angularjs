angular
  .module('InvoiceEditor', [])
  .controller('InvoiceEditorCtrl', (
    $scope, InvoiceService
  ) => {

    $scope.invoice = InvoiceService.getInvoice();

    $scope.addPosition = (index) => {
      InvoiceService.addPosition(index);
    };

    $scope.removePosition = (index) => {
      InvoiceService.removePosition(index);
    };

    $scope.changeClient = client => {
      InvoiceService.setClient(client);
    };

  });
